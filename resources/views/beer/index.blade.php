@extends('layout')

@section('content')
    <form action="{{ route('beer.search') }}" method="POST">
        {{ csrf_field() }}
        <div class="container">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="keyword" placeholder="Search...">
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-block btn-primary"><span class="glyphicon glyphicon-search"></span> Search</button>
                        </div>
                        <div class="col-md-2">
                            <a href="{{ route('beer.random') }}" class="btn btn-block btn-default"><span class="glyphicon glyphicon-refresh"></span> Random</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="container">
        <div class="row">
            @foreach($pagination->items() as $beer)
                <div class="col-md-4">
                    <a href="{{ route('beer.show', $beer->id) }}" class="beer">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">{{ $beer->name }}</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <img src="{{ $beer->image_url }}" class="beer-image img-responsive" alt="">
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                {{ $pagination->links() }}
            </div>
        </div>
    </div>
@endsection