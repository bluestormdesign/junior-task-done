@extends('layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <img src="{{ $beer->image_url }}" class="img-responsive" alt="">
            </div>
            <div class="col-md-10">
                <h1>{{ $beer->name }}</h1>
                <h3>{{ $beer->tagline }}</h3>
                <p class="lead">{{ $beer->description }}</p>

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="col-md-2">ABV</th>
                            <td class="col-md-2">{{ $beer->abv }}%</td>
                            <th class="col-md-2">IBU</th>
                            <td class="col-md-2">{{ $beer->ibu }}</td>
                            <th class="col-md-2">PH Level</th>
                            <td class="col-md-2">{{ $beer->ph }}</td>
                        </tr>
                    </thead>
                </table>

                <div class="info">
                    <h4>First Brewed</h4>
                    <p>{{ $beer->first_brewed }}</p>
                </div>

                <div class="info">
                    <h4>Goes Well With</h4>
                    <p>{{ implode(', ', $beer->food_pairing) }}</p>
                </div>

                <div class="info">
                    <h4>Brewers Tips</h4>
                    <p>{{ $beer->brewers_tips }}</p>
                </div>

                <h4>Ingredients (Makes {{ $beer->volume->value }} {{ $beer->volume->unit }})</h4>

                @foreach($beer->ingredients as $type => $typeIngredients)
                    <h5>{{ ucwords($type) }}</h5>

                    @if(is_array($typeIngredients))
                        <table class="table table-bordered table-condensed">
                            <thead>
                            <tr>
                                <th class="col-md-4">Name</th>
                                <th class="col-md-8">Measurement</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($typeIngredients as $ingredient)
                                <tr>
                                    <td>{{ $ingredient->name }}</td>
                                    <td>{{ $ingredient->amount->value }} {{ $ingredient->amount->unit }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <p>{{ $typeIngredients }}</p>
                    @endif
                @endforeach

                {{--<pre>
                    {{ print_r($beer) }}
                </pre>--}}
            </div>
        </div>
    </div>
@endsection